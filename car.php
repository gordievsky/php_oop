<?php

class Car
{
  private $color;
  private $weight;

  public function __set($weight, $value) {
    if (property_exists($this, $weight)) {
      $this->$weight = $value;
    }
  }
  public function __get($propName) {
    if (property_exists($this, $propName)) {
      return $this->$propName;
    }
  }
  
}

$mustang = new Car();
$mustang->color = "blue";
$mustang->weight = '1500 Kg';
$mustangWeight = $mustang->weight;
$mustangColor = $mustang->color;
echo 'My car has ' . $mustangColor . ' color<br>';
echo 'My car has weight is ' . $mustangWeight . '<br>';
