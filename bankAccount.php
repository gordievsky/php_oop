<?php

class Account
{
  protected $accountNumber;
  protected $totalBalance;
}
class BankAccount extends Account
{
  public function __construct($accNum, $balance)
  {
    $this->accountNumber = $accNum;
    $this->totalBalance = $balance;
    echo '<hr>This is a new bank account<br>';
  }
}

$myBankAccount = new BankAccount(13, 1000000);
print_r($myBankAccount);
print_r('<br>');
