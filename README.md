## PHP ООП.
## Урок 11. Абстрактные и финальные методы и классы

#### Необходимо:
1. Ознакомиться с предыдущими уроками

#### Ход урока:
1. Перейдите на ветку одиннадцатого урока командой ```git checkout lesson11 --force```
2. Изучите файл ```vendor/liw/core/Application.php``` и найдите образец абстрактного метода, финального метода
3. Может ли класс ```liw\core\Application``` быть объявлен не абстрактным? Почему?
4. Откройте файл ```web/index.php``` и расскоментируйте строки, которые пытаются создать экземпляр абстрактного класса. Запустите код. Что произошло?
5. Попробуйте создать пару своих классов и создайте их экземпляры. Используйте асбтрактные классы, финальные методы и наследование
6. Переходите к [Уроку 12. Автоматическая загрузка (autoload) классов](https://github.com/altiore/mm/tree/lesson12)

## Список уроков:
1. [Введение. Тестируем локальный сервер](https://github.com/altiore/mm/tree/lesson1)
2. [Класс (class). Свойства и методы класса](https://github.com/altiore/mm/tree/lesson2)
3. [Область видимости свойств и методов класса (private, public, protected)](https://github.com/altiore/mm/tree/lesson3)
4. [Статические (static) свойства и методы](https://github.com/altiore/mm/tree/lesson4)
5. [Ключевые слова $this, self, static](https://github.com/altiore/mm/tree/lesson5)
6. [Магические методы (__construct(), __invoke()... )](https://github.com/altiore/mm/tree/lesson6)
7. [Типичная структура рабочих папок](https://github.com/altiore/mm/tree/lesson7)
8. [Пространство имен (namespace)](https://github.com/altiore/mm/tree/lesson8)
9. [Наследование (extends), ключевое слово "parent::"](https://github.com/altiore/mm/tree/lesson9)
10. [Полиморфизм и инкапсуляция - сложные названия простых вещей](https://github.com/altiore/mm/tree/lesson10)
11. [Абстрактные (abstract) и финальные (final) классы и методы](https://github.com/altiore/mm/tree/lesson11)
12. [Автоматическая загрузка (autoload) классов](https://github.com/altiore/mm/tree/lesson12)
13. [Интерфейс (interface). Сходства с абстрактным классом, различия](https://github.com/altiore/mm/tree/lesson13)
14. [Трэйт (trait)](https://github.com/altiore/mm/tree/lesson14)
15. [Обработка ошибок (error hendler), исключения (exceptions)](https://github.com/altiore/mm/tree/lesson15)
16. [Замыкания (closure)](https://github.com/altiore/mm/tree/lesson16)

