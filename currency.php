<?php

class Currency
{
  private $myCurrency = 'USD';
  public function __construct($currency)
  {
    $this->myCurrency = $currency;
    echo '<hr>instance of class Currency<br>';
  }
  public function __toString()
  {
    return $this->myCurrency;
  }
}

$myCurrency = new Currency('EUR');
//print_r($myCurrencyObj);
echo 'myCurrency is ' . $myCurrency;